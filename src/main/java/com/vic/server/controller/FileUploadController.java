package com.vic.server.controller;

import com.vic.parser.MetricFileParser;
import com.vic.parser.exception.ParserException;
import com.vic.parser.model.PackageMetric;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;

@Slf4j
@RestController
public class FileUploadController {

    @PostMapping("/uploadFile1")
    public String uploadFile1(@RequestParam("file") MultipartFile file) throws IOException, ParserException {
        log.debug("endpoint: /uploadFile1");
        log.debug("filename = {}", file.getOriginalFilename());
        PackageMetric packageMetric = MetricFileParser.parseArray(file.getBytes());
        log.debug("packageMetric = {}", packageMetric);
        return "OK";
    }

    @PostMapping("/uploadFile2")
    public String uploadFile2(InputStream is) throws IOException, ParserException {
        log.debug("endpoint: /uploadFile2");
        PackageMetric packageMetric = MetricFileParser.parseStream(is);
        log.debug("packageMetric = {}", packageMetric);
        return "OK";
    }

}
